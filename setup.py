#-*-coding:utf-8-*-
from setuptools import setup, find_packages

setup(
    name="atman_varaint_config",
    version="1.0",
    author="Atman",
    description="a config manager package for atman team",
    packages=find_packages(),
    author_email="mli@atman360.com",
    install_requires=[
        "configparser"
    ]
)
